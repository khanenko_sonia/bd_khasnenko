﻿using RailwwayDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RailwwayDB.Controllers
{
    public class PaymentTypeAnalyticsController : Controller
    {
        private railway_khanenko_ist221Entities db = new railway_khanenko_ist221Entities(); 

        // GET: PaymentTypeAnalytics
        public ActionResult PaymentTypeAnalytics()
        {
            return View();
        }

        // GET: PaymentTypeAnalytics/PaymentTypeAnalyticsData
        public JsonResult PaymentTypeAnalyticsData()
        {
            var paymentTypes = db.Payments
                                .GroupBy(p => p.type)
                                .Select(g => new { type = g.Key, count = g.Count() })
                                .ToList();

            var labels = paymentTypes.Select(pt => pt.type).ToArray();
            var data = paymentTypes.Select(pt => pt.count).ToArray();

            return Json(new { labels, data }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}