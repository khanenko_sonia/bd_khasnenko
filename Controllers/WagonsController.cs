﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RailwwayDB.Models;

namespace RailwwayDB.Controllers
{
    public class WagonsController : Controller
    {
        private railway_khanenko_ist221Entities db = new railway_khanenko_ist221Entities();

        // GET: Wagons
        public ActionResult Index()
        {
            var wagons = db.Wagons.Include(w => w.Trains);
            return View(wagons.ToList());
        }

        // GET: Wagons/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Wagons wagons = db.Wagons.Find(id);
            if (wagons == null)
            {
                return HttpNotFound();
            }
            return View(wagons);
        }

        // GET: Wagons/Create
        public ActionResult Create()
        {
            ViewBag.train_id = new SelectList(db.Trains, "train_id", "train_name");
            return View();
        }

        // POST: Wagons/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "wagon_id,train_id,Number,Class")] Wagons wagons)
        {
            if (ModelState.IsValid)
            {
                db.Wagons.Add(wagons);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.train_id = new SelectList(db.Trains, "train_id", "train_name", wagons.train_id);
            return View(wagons);
        }

        // GET: Wagons/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Wagons wagons = db.Wagons.Find(id);
            if (wagons == null)
            {
                return HttpNotFound();
            }
            ViewBag.train_id = new SelectList(db.Trains, "train_id", "train_name", wagons.train_id);
            return View(wagons);
        }

        // POST: Wagons/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "wagon_id,train_id,Number,Class")] Wagons wagons)
        {
            if (ModelState.IsValid)
            {
                db.Entry(wagons).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.train_id = new SelectList(db.Trains, "train_id", "train_name", wagons.train_id);
            return View(wagons);
        }

        // GET: Wagons/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Wagons wagons = db.Wagons.Find(id);
            if (wagons == null)
            {
                return HttpNotFound();
            }
            return View(wagons);
        }

        // POST: Wagons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Wagons wagons = db.Wagons.Find(id);
            db.Wagons.Remove(wagons);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
