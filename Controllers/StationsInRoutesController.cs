﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RailwwayDB.Models;

namespace RailwwayDB.Controllers
{
    public class StationsInRoutesController : Controller
    {
        private railway_khanenko_ist221Entities db = new railway_khanenko_ist221Entities();

        // GET: StationsInRoutes
        public ActionResult Index()
        {
            var stationsInRoutes = db.StationsInRoutes.Include(s => s.Stations).Include(s => s.Trains);
            return View(stationsInRoutes.ToList());
        }

        // GET: StationsInRoutes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StationsInRoutes stationsInRoutes = db.StationsInRoutes.Find(id);
            if (stationsInRoutes == null)
            {
                return HttpNotFound();
            }
            return View(stationsInRoutes);
        }

        // GET: StationsInRoutes/Create
        public ActionResult Create()
        {
            ViewBag.station_id = new SelectList(db.Stations, "station_id", "station_name");
            ViewBag.train_id = new SelectList(db.Trains, "train_id", "train_name");
            return View();
        }

        // POST: StationsInRoutes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "StationsInRoutes_id,train_id,station_id,DepatureDateTime,ArrivalDateTime")] StationsInRoutes stationsInRoutes)
        {
            if (ModelState.IsValid)
            {
                db.StationsInRoutes.Add(stationsInRoutes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.station_id = new SelectList(db.Stations, "station_id", "station_name", stationsInRoutes.station_id);
            ViewBag.train_id = new SelectList(db.Trains, "train_id", "train_name", stationsInRoutes.train_id);
            return View(stationsInRoutes);
        }

        // GET: StationsInRoutes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StationsInRoutes stationsInRoutes = db.StationsInRoutes.Find(id);
            if (stationsInRoutes == null)
            {
                return HttpNotFound();
            }
            ViewBag.station_id = new SelectList(db.Stations, "station_id", "station_name", stationsInRoutes.station_id);
            ViewBag.train_id = new SelectList(db.Trains, "train_id", "train_name", stationsInRoutes.train_id);
            return View(stationsInRoutes);
        }

        // POST: StationsInRoutes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "StationsInRoutes_id,train_id,station_id,DepatureDateTime,ArrivalDateTime")] StationsInRoutes stationsInRoutes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(stationsInRoutes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.station_id = new SelectList(db.Stations, "station_id", "station_name", stationsInRoutes.station_id);
            ViewBag.train_id = new SelectList(db.Trains, "train_id", "train_name", stationsInRoutes.train_id);
            return View(stationsInRoutes);
        }

        // GET: StationsInRoutes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StationsInRoutes stationsInRoutes = db.StationsInRoutes.Find(id);
            if (stationsInRoutes == null)
            {
                return HttpNotFound();
            }
            return View(stationsInRoutes);
        }

        // POST: StationsInRoutes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            StationsInRoutes stationsInRoutes = db.StationsInRoutes.Find(id);
            db.StationsInRoutes.Remove(stationsInRoutes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
