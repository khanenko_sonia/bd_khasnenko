﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace RailwwayDB
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static Timer backupTimer; 

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            
            ScheduleDailyBackup();
        }

        private void ScheduleDailyBackup()
        {
            var now = DateTime.Now;
            var nextRunTime = new DateTime(now.Year, now.Month, now.Day, 2, 0, 0, 0); 

            if (now > nextRunTime)
            {
                nextRunTime = nextRunTime.AddDays(1); 
            }

            var timeToGo = nextRunTime - now;
            var dueTime = timeToGo.TotalMilliseconds;

            backupTimer = new Timer(x =>
            {
                PerformBackup();
                ScheduleDailyBackup(); 
            }, null, (long)dueTime, Timeout.Infinite);
        }

        private void PerformBackup()
        {
            string serverName = "LAPTOP-D9UUDH3O";
            string databaseName = "RailwayDB";
            string backupDirectory = @"C:\Backups";
            string backupFileName = $"{databaseName}_{DateTime.Now:yyyyMMdd}.bak";
            string backupFilePath = $@"{backupDirectory}\{backupFileName}";

            string sqlCommand = $"BACKUP DATABASE [{databaseName}] TO DISK = N'{backupFilePath}' WITH NOFORMAT, NOINIT, NAME = N'{databaseName}-Full Database Backup', SKIP, NOREWIND, NOUNLOAD, STATS = 10";

            ProcessStartInfo processStartInfo = new ProcessStartInfo
            {
                FileName = "sqlcmd",
                Arguments = $"-S {serverName} -Q \"{sqlCommand}\"",
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true
            };

            Process process = new Process
            {
                StartInfo = processStartInfo
            };

            process.Start();
            process.WaitForExit();
        }
    }
}
